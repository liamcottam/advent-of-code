package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	data, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	// Convert to string and split by line.
	lines := strings.Split(string(data), "\n")

	// Loop Variables
	runningValue := 0
	max := len(lines)
	lineNum := 0
	numLoops := 0

	// Map that contains list of frequencies that have been seen.
	resultDuplicateMap := map[int]int{}
	dupeFound := false

	// Results
	partOneResult := 0
	partTwoResult := 0

	// Run around continuously until a result has been found.
	for {
		val, err := strconv.Atoi(lines[lineNum])
		if err != nil {
			panic(err)
		}

		if !dupeFound {
			resultDuplicateMap[runningValue]++
			if resultDuplicateMap[runningValue] == 2 {
				dupeFound = true
				partTwoResult = runningValue
			}
		}

		runningValue += val

		if numLoops < 1 {
			partOneResult += val
		}

		if dupeFound && numLoops >= 1 {
			break
		}

		lineNum++
		if lineNum == max {
			lineNum = 0
			numLoops++
		}
	}

	// Print the result!
	fmt.Printf("Part 1 Result: %d\n", partOneResult)
	fmt.Printf("Part 2 Result: %d (found after %d loops)\n", partTwoResult, numLoops)
}
