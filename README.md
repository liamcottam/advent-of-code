# Advent of Code Golang Solutions

This contains Golang solutions for the Advent of Code Challenge hosted [here](https://adventofcode.com/).

Please remember that the input data is specific to me.
