package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type Parser struct {
	Items    []string
	Position int
	Metadata int
}

type Node struct {
	Children []*Node
	Metadata []int
	Value    int
}

func (p *Parser) Next() int {
	result, _ := strconv.Atoi(p.Items[p.Position])
	p.Position++
	return result
}

func (p *Parser) Parse() *Node {
	n := &Node{
		Children: make([]*Node, p.Next()),
		Metadata: make([]int, p.Next()),
	}
	for i := 0; i < len(n.Children); i++ {
		n.Children[i] = p.Parse()
	}
	for i := 0; i < len(n.Metadata); i++ {
		m := p.Next()
		n.Metadata[i] = m
		p.Metadata += m

		if len(n.Children) == 0 {
			n.Value += m
		} else if m-1 < len(n.Children) {
			n.Value += n.Children[m-1].Value
		}
	}

	return n
}

func main() {
	data, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	items := strings.Split(string(data), " ")
	p := Parser{
		Items: items,
	}
	rootNode := p.Parse()

	fmt.Printf("Answer One: %d\n", p.Metadata)
	fmt.Printf("Answer Two: %d\n", rootNode.Value)
}
