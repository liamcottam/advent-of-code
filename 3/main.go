package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

const ()

func main() {
	data, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	grid := make(map[int][]int, 1000*1000)

	// Convert to string and split by line.
	lines := strings.Split(string(data), "\n")

	collisions := map[int]bool{}
	numCollisions := 0
	nonCollidingIDs := map[int]int{}

	for _, line := range lines {

		parts := strings.Split(line, " ")

		id, err := strconv.Atoi(strings.TrimPrefix(parts[0], "#"))
		if err != nil {
			panic(err)
		}

		startPosition := strings.Split(strings.TrimSuffix(parts[2], ":"), ",")
		startX, err := strconv.Atoi(startPosition[0])
		if err != nil {
			panic(err)
		}
		startY, err := strconv.Atoi(startPosition[1])
		if err != nil {
			panic(err)
		}

		size := strings.Split(parts[3], "x")
		width, err := strconv.Atoi(size[0])
		if err != nil {
			panic(err)
		}
		height, err := strconv.Atoi(size[1])
		if err != nil {
			panic(err)
		}

		endX := startX + width
		endY := startY + height

		hasCollision := false
		for x := startX; x < endX; x++ {
			for y := startY; y < endY; y++ {
				pos := 1000*x + y
				if len(grid[pos]) >= 1 {
					hasCollision = true
					nonCollidingIDs[id] = 2
					for _, gridID := range grid[pos] {
						nonCollidingIDs[gridID] = 2
					}
					if !collisions[pos] {
						collisions[pos] = true
						numCollisions++
					}
				}
				grid[pos] = append(grid[pos], id)
			}
		}
		if !hasCollision && nonCollidingIDs[id] != 2 {
			nonCollidingIDs[id] = 1
		}
	}

	nonCollidingID := 0
	for k, v := range nonCollidingIDs {
		if v == 1 {
			nonCollidingID = k
		}
	}

	fmt.Printf("Result One: %d\n", numCollisions)
	fmt.Printf("Result Two: %d\n", nonCollidingID)
}
