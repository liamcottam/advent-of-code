package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

func main() {
	data, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	// Convert to string and split by line.
	lines := strings.Split(string(data), "\n")

	// Sort the lines so the closer matching strings are close, then I can just
	// check against the previous one.
	sort.Strings(lines)

	numTwos := 0
	numThrees := 0

	type matchStruct struct {
		Matching []byte
		Matches  int
	}

	match := matchStruct{
		Matches: -1,
	}

	for i := 0; i < len(lines); i++ {
		characterUniqMap := map[byte]int{}
		hasThree := false
		hasTwo := false

		line := lines[i]

		// Probably inefficient, should use a state but I'm late to the party.
		var prevLine *string
		matches := 0
		matching := []byte{}
		if i > 1 {
			prevLine = &lines[i-1]
		}

		for j := 0; j < len(line); j++ {
			characterUniqMap[line[j]]++

			if prevLine != nil {
				if line[j] == (*prevLine)[j] {
					matches++
					matching = append(matching, line[j])
				}
			}
		}

		if prevLine != nil && (match.Matches == -1 || matches > match.Matches) {
			match.Matches = matches
			match.Matching = matching
		}

		for _, v := range characterUniqMap {
			if !hasTwo && v == 2 {
				numTwos++
				hasTwo = true
			}
			if !hasThree && v == 3 {
				numThrees++
				hasThree = true
			}
		}
	}

	fmt.Printf("Result One: %d\n", numTwos*numThrees)
	fmt.Printf("Result Two: %s\n", string(match.Matching))
}
